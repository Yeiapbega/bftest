require('express-router-group')
const express = require('express')
const path = require('path');
const router = express.Router();

require('dotenv').config()
// var cryptorjs = require('cryptorjs');
// var myCryptor = new cryptorjs(process.env.CRYPTOR_PASS);
var { encode, decode } = require('../Modules/Utilities/Plugins/cryptor copy')

//session validator, if this function return true the user will be redirected at local dashboard
var Checker = function (req, res, next) {
    if (req.session.user && req.cookies.user_session_id) {
        res.redirect('/');
    } else {
        res.clearCookie('token');   
        // res.cookie('token', null, {expire: 'Thu, 01 Jan 1970 00:00:01 GMT'})     
        next();
    }    
};

router.get('/login', Checker, (req, res) => {
    // res.clearCookie('token');
    res.sendFile(path.join(__dirname, '../../public/login.html'))
})

router.post('/sessdata', (req,res) => 
{
    if (req.session.user && req.cookies.user_session_id) {
        let temp = req.session.user;
        let data = [];
        for (const key in req.body.fields) {
            let a = req.body.fields[key];
            let ths = temp[a];
            data.push(ths)
        }
        let resp = encode(data)
        res.json({data: resp});
    }
    else {
        res.json('failed')
    }
})

router.post('/validate', async (req, res) => {
        let dataAuth = (req.body.data) //decode data        
        let authToken = req.body.data.sessions.access_token        
        let vali = false       
        let status = 'error al iniciar sesión'
        if(authToken != '')
        {
            res.cookie('token', `${authToken}`);
        }
        if(dataAuth)
        {            
            let datitoss = dataAuth    
            req.session.user = datitoss

            if(req.session && req.session.user)
            {
                vali = true
                status = 'Authenticated!'
            }
        } else {
        vali = false
       }
        return res.json({validation:vali, info:status})
    })

module.exports = router