window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.$ = window.jQuery = require('jquery');
$('[data-toggle="tooltip"]').tooltip()
// window.dt = require( 'datatables.net' )();

window.Vue = require('vue');
let cryptorjs = require('./Utilities/Plugins/cryptor');
let chidoriAlert = require('./Utilities/Plugins/chidoriAlert');
let swal = require('./Utilities/Plugins/sweetAlert')

import VueRouter from 'vue-router';
import picker from 'vuejs-datepicker';

import vSelect from 'vue-select';
import VModal from 'vue-js-modal';
 
Vue.use(VModal, { dialog: true })

Vue.use(picker);
Vue.use(VueRouter);

//pagination 
import VuePaginate from 'vue-paginate';
Vue.use(VuePaginate); 

//data table UI
//Source
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
//language
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
const DataTables = require('vue-data-tables') 

locale.use(lang)
Vue.use(DataTables)

import parallax from './SF/Components/ParallaxComponent'
import createA from './SF/Components/createAreas'
import listA from './SF/Components/listAreas'

let paths = 
{
  createA,
  listA,
  parallax
}


let sfroutes = 
[  
  { path: '/', component: paths.parallax },
  { path: '/bf/create-area', component: paths.createA },
  { path: '/bf/list-areas', component: paths.listA },
  // { path: '/vc/sf/update/reception/:id?', component: paths.SFreceptionUp},    
]  
  const sfrouter = new VueRouter({
    uid: 'sf',
    // mode: 'history',   
    routes: sfroutes // short for `routes: routes`
  })

Vue.component('bf-component', require('./SF/Components/MainComponent.vue').default);
Vue.component('login-component', require('../common/Components/LoginComponent.vue').default);
Vue.component('v-select', vSelect)

const mainapp = new Vue({
    el: '#main'
});
const app = new Vue({
  el: '#app'
});
const oapp = new Vue({
    el: '#bf',
    router: sfrouter
});
