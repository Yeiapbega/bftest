let show = function(title, text, type, time)
{   
    $('#soundNotify')[0].play()
    $('#chidoriAlert .iconAlert').removeClass('animated bounceIn')
    if(type == 'success')
    {
        $('#chidoriAlert').css({'bottom':'3%'})   
        $('#chidoriAlert').removeClass('error').addClass(type)
        $('#chidoriAlert .textAlert strong').text(title).removeClass('error').addClass(type)
        $('#chidoriAlert .textAlert span').text(text)
        $('#chidoriAlert > div .alertButton').removeClass('error').addClass(type) 
        setTimeout(() => {
            $('#chidoriAlert .iconAlert').attr('src', 'http://localhost:3000/src/textures/icons/' + type + '.png').addClass('animated bounceIn')
        }, 200);
        $('#chidoriAlert').css({
            'bottom': '3%'
        })
    }
    if(type == 'error')
    {
        $('#chidoriAlert').removeClass('success').addClass(type)
        $('#chidoriAlert .textAlert strong').text(title).removeClass('success').addClass(type)
        $('#chidoriAlert .textAlert span').text(text)
        $('#chidoriAlert > div .alertButton').removeClass('success').addClass(type)   
        setTimeout(() => {
            $('#chidoriAlert .iconAlert').attr('src', 'http://localhost:3000/src/textures/icons/' + type + '.png').addClass('animated bounceIn')
        }, 200);
        $('#chidoriAlert').css({
            'bottom': '3%'
        })
    }
    if(time)
    {
        setTimeout(() => {
            $('#chidoriAlert').css({
                'bottom': '-100%'
            })
        }, time);
    }
}
module.exports = {show};