const axios = require('axios')

// axios.defaults.baseURL = 'https://sandbox.datill.co/api/api/V1/'

const getCookie = name => {
  let v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)')  
  return v ? v[2] : null
}

const header = (method, url, isAuth, data, toSendAFile, _module) => 
{
    let options = { method }      

    options.url = url    
    data && (options.data = data)
    // options.headers = {'Access-Control-Allow-Origin': '*', crossdomain: true, 'Content-Type': toSendAFile ? 'multipart/form-data' : 'application/json', 'XRequestedWith':'XMLHttpRequest'}
  
    if(_module == 'SF')
    {
      axios.defaults.baseURL = 'http://localhost:3000/'
    } else {
      axios.defaults.baseURL = 'https://sandbox.datill.co/api/api/V1/'
    }

    // if (typeof isAuth !== 'boolean' || !isAuth) 
    // {            
    //   // options.headers = {...options.headers, 'x-m-i': }      
    // } else
    if (isAuth) 
    {
      const token = document && getCookie('token')
      token && (options.headers = {'Authorization': `Bearer ${token}`})
    }
    return options
}
  
export default (method, url, isAuth = false, data = null, toSendAFile = false, _module = false) =>
new Promise((resolve, reject) => 
{
    axios(header(method, url, isAuth, data, toSendAFile, _module))
    .then(res => resolve(res))
    .catch(err => reject(err))
})
  