// const cryp = require('cryptorjs')
// var myCryptor = new cryp('KaioKenx100alafergatodo');

// const crypPlugin = {}

// crypPlugin.install = function(Vue)
// {
// 	Vue.prototype.$myCryptor = myCryptor
// }

// Vue.use(crypPlugin)

var CryptoTS = require("crypto-ts");
var key = 'kaiokenx200alafergax100alafergax50alaferga';
const cryptsPlugin = {}

cryptsPlugin.install = function(Vue)
{
    Vue.prototype.$myCryptor = {
        encode: (data) => {
         return CryptoTS.AES.encrypt(JSON.stringify(data), key).toString()   
        },
        decode: (data) => {
         return JSON.parse(CryptoTS.AES.decrypt(data.toString(), key).toString(CryptoTS.enc.Utf8))  
        }
    }
}

Vue.use(cryptsPlugin)