const express = require('express');
const mongoose = require('mongoose')
const session = require('express-session')
const cParser = require('cookie-parser')
const path = require('path')
const fs = require('fs')
const axios = require('axios');

require('dotenv').config()

// const cookies = requirejs('cookie.js')
const updateJsonFile = require('update-json-file')
var cryptorjs = require('cryptorjs')

const app = express();
const http = require('http').Server(app)

//Settings
app.set('port', process.env.PORT || 3000)

//Middlewares
app.use(express.json());
app.use(cParser())
app.use(session({
    key: 'user_session_id',
    secret: 's3cr3tc0d3',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: (60*60*1000)
    }
}))

//Routes
const middleware = function (req, res, next) {
    if (!(req.session.user && req.cookies.user_session_id)) {
        res.redirect('/auth/login');
    } else {
        next();
    }    
};

app.get('/', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.sendFile(path.join(__dirname, '/public/sf.html'));
        // res.sendFile(path.join(__dirname, '/public/sf.html'));
    } else {
        res.clearCookie('token');   
        res.cookie('token', null, {expire: 'Thu, 01 Jan 1970 00:00:01 GMT'})     
        res.redirect('/auth/login');
    }
})

app.get('/logout', (req, res) => {    
    res.clearCookie('token')
    let user = '';
    // if(req.session.user){
    //     user = req.session.user.dni.dni
    // }    
    req.session.user = null 
    res.redirect('/auth/login');
})

app.get('/vc/sf/:par', (req, res) => {res.redirect('/sf')})
app.get('/sf', middleware,(req, res) => {res.sendFile(__dirname + '/public/sf.html')})

// app.use('/sf', require('./src/Routes/sf'))
// app.use('/api', require('./src/Routes/api'))
app.use('/auth', require('./src/Routes/auth'))

//404 default response
// app.use(function (req, res, next) {
//     res.status(404).send("Sorry we can't find that!")
// });

//Static Path
app.use(express.static(__dirname + '/public'))

//Server Listener
http.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})